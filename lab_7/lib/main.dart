import 'package:lab_7/form_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: MyApp(),
    theme: themeData,
  ));
}

final ThemeData themeData = ThemeData(
  accentColor: Colors.blue,
);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 7',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: const MyHomePage(title: 'Safe Flight'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      body: Center(
        child: FlatButton(
          onPressed: () {
            Navigator.push(ctx, PageTwo());
          },
          child: Text("Ada Keluhan? Klik ini untuk Laporkan Keluhanmu"),
        ),
      ),
    );
  }
}


class PageTwo extends MaterialPageRoute<StatelessWidget> {
  PageTwo()
      : super(builder: (BuildContext ctx) {
          return MaterialApp(
            title: 'Bantuan',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: FormScreen(),
          );
        });
}
