import 'package:lab_6/form_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: MyApp(),
    theme: themeData,
  ));
}

final ThemeData themeData = ThemeData(
  accentColor: Colors.blue,
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      body: Center(
        child: FlatButton(
          onPressed: () {
            Navigator.push(ctx, PageTwo());
          },
          child: Text("Ada Keluhan? Klik ini untuk Laporkan Keluhanmu"),
        ),
      ),
    );
  }
}

class PageTwo extends MaterialPageRoute<StatelessWidget> {
  PageTwo()
      : super(builder: (BuildContext ctx) {
          return MaterialApp(
            title: 'Bantuan',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: FormScreen(),
          );
        });
}
