1. Apakah perbedaan antara JSON dan XML?


JSON (JavaScript Object Notation) merupakan suatu format yang ditulis dengan bahasa JavaScript, biasanya JSON digunakan untuk merepresentasikan object dan bekerja dengan cepat karena ukuran filenya yang kecil. Karena itu XML juga cocok digunakan pada format pertukaran data karena mudah untuk diurai, formatnya yang tidak menggunakan end tag juga membuat JSON lebih mudah dibaca dibanding XML.


XML (Extensible Markup Language) ukurannya cukup besar karena biasanya menggunakan tag untuk mendefinisikan suatu elemen, XML juga cukup lambat dalam penguraiannya dibanding JSON karena ukuran filenya yang cukup besar, XML memiliki start dan end tag dan mengandung root element yang merupakan parent dari elemen lainnya, XML juga termasuk dalam case sensitive, sehingga perlu berhati-hati dalam penggunaannya.


2. Apakah perbedaan antara HTML dan XML?


HTML (Hyper Text Markup Language) biasa digunakan untuk menampilkan suatu konten, HTML juga tidak bersifat case sensitve artinya HTML mengabaikan whitespace yang tidak terformat tidak seperti XML, HTML secara tidak langsung mendeskripsikan struktur dari sebuah halaman web.


XML sendiri sebenarnya juga banyak digunakan pada aplikasi web maupun mobile, namun berbeda dengan HTML, dalam penulisan XML perlu berhati-hati karena XML bersifat case sensitive sehingga dapat mendeteksi whitespace. Dan karena XML secara tidak langsung adalah suatu informasi yang sebenarnya dibungkus dalam tag, XML ini sendiri didesain self-descriptive sehingga user dapat membaca dan mengerti informasi yang ingin disampaikan data.
