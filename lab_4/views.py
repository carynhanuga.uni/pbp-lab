from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    # create object of form
    form = NoteForm(request.POST or None)

    # check if form data is valid (prevent from SQL injection, too)
    if form.is_valid() and request.method == 'POST':
        # save the form data to the model
        form.save()

        return HttpResponseRedirect("/lab-4")
    
    context = {'form': form}
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
