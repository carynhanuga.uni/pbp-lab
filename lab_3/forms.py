from django.forms import ModelForm, DateInput, widgets
from lab_1.models import Friend

class InputDOB(DateInput):
    input_type = 'date'

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
        widgets = {'dob': InputDOB()}
