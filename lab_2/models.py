from django.db import models

# Create your models here.
class Note(models.Model):
    note_to = models.CharField(max_length=1024)
    note_from = models.CharField(max_length=1024)
    title = models.CharField(max_length=1024)
    message = models.TextField()